class Solution:
    def maxProfit(self, prices: List[int]) -> int:

        buy_ptr = 0
        sell_ptr = 1

        max_profit = 0

        while sell_ptr < len(prices):
            current_profit = prices[sell_ptr] - prices[buy_ptr]

            if current_profit < 0:
                buy_ptr = sell_ptr
                sell_ptr += 1
            else:
                max_profit = max(max_profit, current_profit)
                sell_ptr +=1

        return max_profit