class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        
        if (len(s) == 0):
            # edge-case
            return 0

        left_ptr, right_ptr = 0, 0
        res = 0

        substr_set = set()

        while right_ptr < len(s):
            if s[right_ptr] not in substr_set:
                substr_set.add(s[right_ptr])
            else:
                # duplicate found so do the following
                while(s[right_ptr] in substr_set):
                    substr_set.remove(s[left_ptr])
                    left_ptr += 1
                
                substr_set.add(s[right_ptr])

            right_ptr += 1
            res = max(res, len(substr_set))
        
        return res