class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:

        # Approach 1. Sorting based

        str_map = collections.defaultdict(list)

        for s in strs:
           str_map[tuple(sorted(s))].append(s)

        return str_map.values()


        # Approach 2. Unicode based
        NUM_LOWERCASE_LETTERS = 26

        str_map = collections.defaultdict(list)

        for s in strs:
            count = [0] * NUM_LOWERCASE_LETTERS
            # populate the count array
            for char in s:
                count[ord(char) - ord('a')] += 1
            
            str_map[tuple(count)].append(s)

        return str_map.values()