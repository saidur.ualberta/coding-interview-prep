class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """

        # Apporach. One-pass HashMap based

        nums_hash = {}

        for idx, value in enumerate(nums):
            if value not in nums_hash:
                nums_hash[target - value] = idx
            elif value in nums_hash:
                return [idx, nums_hash[value]]
               