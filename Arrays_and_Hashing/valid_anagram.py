class Solution:
    def isAnagram(self, s: str, t: str) -> bool:

        # Approach 1. HashMap based

        s_map = {}
        t_map = {}

        for char in s:
            s_map[char] = 1 + s_map.get(char, 0)
        for char in t:
            t_map[char] = 1 + t_map.get(char, 0)

        return s_map == t_map


        # Approach 2. Sorting based
        s_sort = sorted(s)
        t_sort = sorted(t)

        return s_sort == t_sort