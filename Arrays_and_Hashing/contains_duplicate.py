class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:

        # Apporach 2. Sorting based
        nums.sort()

        for i in range(len(nums)-1):
            if (nums[i] == nums[i+1]):
                return True
        
        return False
        
        # Approach 3. Hashmap based

        hash_map = {}

        for val in nums:
            hash_map[val] = 1 + hash_map.get(val, 0)
            
        for count in hash_map.values():
            if count > 1:
                return True

        return False
        
        
        # Approach 4. HashSet based
        hash_set = set()

        for val in nums:
			if val in hash_set:
				return True
			else:
				hash_set.add(val)

        #return False