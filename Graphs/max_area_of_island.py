class Solution:
    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:

        ROWS, COLS = len(grid), len(grid[0])

        visit = set()
        
        area = 0
        dirs = [[-1, 0], [1, 0], [0, -1], [0, 1]]

        def dfs(row,col, area):
            
            '''
            DFS recursive solution
            '''
            
            if (row not in range(ROWS) or 
            col not in range(COLS) or 
            grid[row][col] == 0 or
            (row,col) in visit):
                return area

            visit.add((row,col))
            area += 1

            area = dfs(row+1, col, area) # up 
            area = dfs(row-1, col, area) # down
            area = dfs(row, col+1, area) # left
            area = dfs(row, col-1, area) # right
            
            return area

        def bfs(row, col, area):
        
            '''
            BFS solution
            '''
            
            q = deque()
            
            q.append((row, col))
            visit.add((row, col))
            area += 1

            while q:
                row, col = q.popleft()

                for dir_r, dir_c in dirs:
                    r = row + dir_r
                    c = col + dir_c

                    if (r in range(ROWS) and
                    c in range(COLS) and
                    grid[r][c] == 1 and
                    (r,c) not in visit
                    ):
                        q.append((r,c))
                        visit.add((r,c))
                        area += 1
            
            return area

        for row in range(ROWS):
            for col in range(COLS):
                if (grid[row][col] == 1 and (row,col) not in visit):
                    # using dfs
                    area = max(dfs(row, col, 0), area)
                    #using bfs
                    area = max(bfs(row, col, 0), area)

        return area