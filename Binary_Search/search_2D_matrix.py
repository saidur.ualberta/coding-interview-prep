class Solution:
    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:
        ROWS = len(matrix)
        COLS = len(matrix[0])

        # row-wise Binary Search
        top = 0
        bottom = ROWS - 1

        while top <= bottom:
            mid_row = (top + bottom) // 2

            if target < matrix[mid_row][0]:
                bottom = mid_row - 1
            elif target > matrix[mid_row][-1]:
                top = mid_row + 1
            else:
                break
        
        # 2 cases: 
        # -> either found the desired row
        # -> we did not find the desired

        if top > bottom:
            return False

        target_row = mid_row

        # column-wise Binary Search

        left = 0
        right = COLS - 1

        while left <= right:
            mid = (left + right) // 2

            if matrix[target_row][mid] == target:
                return True
            else:
                if target < matrix[target_row][mid]:
                    right = mid - 1
                elif target > matrix[target_row][mid]:
                    left = mid + 1
        
        return False