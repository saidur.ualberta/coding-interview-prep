# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def levelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:
        
        res = []

        def bfs(root):
            q = deque([root])
            res.append([root.val])
            while q:
                level_lst = []
                for _ in range(len(q)):
                    node = q.popleft()
                    if node.left:
                        q.append(node.left)
                        level_lst.append(node.left.val)
                    if node.right:
                        q.append(node.right)
                        level_lst.append(node.right.val)
                
                if len(level_lst) > 0: 
                    res.append(level_lst)
        
        if not root:
            return res
        else:
            bfs(root)
            return res