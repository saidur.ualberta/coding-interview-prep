# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def kthSmallest(self, root: Optional[TreeNode], k: int) -> int:
        
        res = []

        # Doing an in-order traversal in a Binary SEARCH tree 
        # returns the values of in ascending (sorted) order
        # So, this problem is trivial and solved as follows:
        # 1. Create an empty array
        # 2. Add 'k' number of node values to the array 
        # 3. Return the last element of that array

        def dfs(root):
            '''
            DFS recursive solution
            '''
            if not root:
                return None

            left_val = dfs(root.left)

            if (len(res) < k):
                res.append(root.val)

            right_val = dfs(root.right)

            return

        # Notice that the above DFS recursive solution traverses the entire tree
        # regardless of when the 'k' th smallest element is found.
        # For example, if k = 1, then 1 (the smallest element in exmaple 1) is found 
        # before even traversing the right subtree of root node (i.e. node 3)
        # So, DFS iterative version allows us to terminate the tree traversal
        # whenever the 'k' th smallest element is found which improves the runtime
        # and it is less than O(n)

        def dfs_iterative(root, k):
            stack = []
            
            node = root

            while stack or node:

                while node:
                    stack.append(node)
                    node = node.left
                
                node = stack.pop()
                k -= 1
                if (k == 0):
                    return node.val

                node = node.right

        
        return dfs_iterative(root, k)
        #dfs(root)        
        #return res[-1]