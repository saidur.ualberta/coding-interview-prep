# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def diameterOfBinaryTree(self, root: Optional[TreeNode]) -> int:

        # the final output result, returns the maximum diameter
        res = [0]
        
        def dfs(root):
            # base-case
            if not root:
                # the height of a leaf node is 0 
                # so None node is assigned to -1
                return -1

            left_height = dfs(root.left)
            right_height = dfs(root.right)

            parent_height = 1 + max(left_height, right_height)
            
            diameter = 2 + left_height + right_height
            res[0] =  max(res[0], diameter)

            return parent_height
        
        dfs(root)
        return res[0]
            