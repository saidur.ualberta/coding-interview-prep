# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isBalanced(self, root: Optional[TreeNode]) -> bool:

        res = [True]

        def dfs(root):
            # base-case
            if not root:
                return -1

            # recursively calling dfs on left and right subtrees 
            left_height = dfs(root.left)
            right_height = dfs(root.right)

            # after the recursive call returns back, the parent does 2 things:

            # 1. It calculates the difference in depth between the left subtree and right subtree
            if abs(left_height - right_height) > 1:
                res[0] = False

            # 2. The parent node (now root) returns its depth to it's own parent
            parent_height = 1 + max(left_height, right_height)

            return parent_height

        if root:
            dfs(root)
        
        return res[0]
