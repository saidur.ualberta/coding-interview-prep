# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def invertTree(self, root: Optional[TreeNode]) -> Optional[TreeNode]:
        
        def dfs_recursive(root):
        
            '''
            DFS recursive
            '''
            
            # base-case
            if not root:
                return None

            # the recursive calls
            self.invertTree(root.left)
            self.invertTree(root.right)

            # post-order operations
            temp = root.left
            root.left = root.right
            root.right = temp
            
            return root

        
        def bfs(root):
                
            '''
            BFS
            '''
            
            q = deque()
            
            if not root:
                return None
            else:
                q.append(root)
                
                while q:
                    for _ in range(len(q)):
                        node = q.popleft()

                        temp = node.left
                        node.left = node.right
                        node.right = temp

                        if node.left:
                            q.append(node.left)
                        if node.right:
                            q.append(node.right)

            return root


        #return dfs_recursive(root)
        return bfs(root)