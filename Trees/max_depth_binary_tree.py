# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def maxDepth(self, root: Optional[TreeNode]) -> int:

        def dfs_recursive(root):
        
            '''
            DFS recursive
            '''
            
            # base-case
            if not root:
                return 0

            
            return 1 + max(dfs_recursive(root.left), 
            dfs_recursive(root.right))

        
        def bfs(root):
        
            '''
            BFS
            '''
            
            depth = 0
            
            if not root:
                return 0

            else:
                q = deque()
                q.append(root)

                while q:
                    for _ in range(len(q)):
                        
                        node = q.popleft()

                        if node.left:
                            q.append(node.left)
                        if node.right:
                            q.append(node.right)
                
                    depth += 1

            return depth
                
        #return dfs_recursive(root)
        return bfs(root)