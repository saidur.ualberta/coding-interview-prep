# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isSameTree(self, p: Optional[TreeNode], q: Optional[TreeNode]) -> bool:        
        
        def dfs(p, q):

            #base-case 1. if both `p` and `q` contain null nodes:
            if not p and not q:
                return True

            #base-case 2. if one of `p` and `q` contain null nodes:
            elif ((p and not q) or (not p and q)):
                return False

            left = dfs(p.left, q.left)
            right = dfs(p.right, q.right)
            
            if (left and right):
                # Now that both left and right children contain nodes,
                # we check if their values are same and then return
                return p.val == q.val
            else:
                return False

        return dfs(p, q)        