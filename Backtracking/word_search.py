class Solution:
    def exist(self, board: List[List[str]], word: str) -> bool:

        ROWS, COLS = len(board), len(board[0])

        path = set() # set to add current values from board to avoid adding same cell

        def dfs(r, c, i):
            # r, c -> position on the board

            # base-case 1:
            if i == len(word):
                return True

            # base-case 2:
            if ((r < 0) or (c < 0) or (r >= ROWS) or (c >= COLS) or
            (word[i] != board[r][c]) or (r,c) in path):
                return False

            path.add((r,c))
            res = (dfs(r+1, c, i+1) or
                    dfs(r-1, c, i+1) or
                    dfs(r, c+1, i+1) or
                    dfs(r, c-1, i+1))


            path.remove((r,c))
            return res

            
        for r in range(ROWS):
            for c in range(COLS):
                if dfs(r, c, 0):
                    # word Found
                    return True

        # the word Not Found
        return False