class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:

        res = [] # final output

        subset = [] # to get global access
        def dfs(i):
            # i tells which element we're currently visiting
            if i >= len(nums):
                # base-case: we're out of bounds
                res.append(subset.copy())
                return

            # decision to include nums[i]
            subset.append(nums[i])
            # non-empty subset will be passed to this `dfs` call
            dfs(i + 1)

            # decision NOT to include nums[i]
            subset.pop()
            # empty subset will be passed to this `dfs` call
            dfs(i + 1)

        dfs(0)
        return res
  