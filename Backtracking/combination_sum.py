class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:

        res = []

        def dfs(i, cur, total):
            # i -> 
            # cur -> what values are added to current combination
            # total -> total sum of the values added 

            # base-case: total reaches the target
            if total == target:
                res.append(cur.copy())
                return

            # base-case: target not found / i reaches out of bounds
            if i >= len(candidates) or total > target:
                return

            cur.append(candidates[i])
            # since `cur` and `total` changed
            dfs(i, cur, candidates[i] + total)

            # we do not include `i`
            cur.pop()
            dfs(i+1, cur, total)

        dfs(0, [], 0)
        return res

