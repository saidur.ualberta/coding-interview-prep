class Solution:
    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]: 

        res = []

        # sort the input array 
        nums.sort()

        def backtrack(i, subset):
            # base-case
            if i == len(nums):
                res.append(subset[::])
                return

            # All subsets that include nums[i]
            subset.append(nums[i])
            backtrack(i + 1, subset)

            # All subsets that don't include nums[i]
            subset.pop() # remove the val added in line 16 

            while i + 1 < len(nums) and nums[i] == nums[i+1]:
                i +=1 
            
            backtrack(i + 1, subset)

        backtrack(0, [])
        return res