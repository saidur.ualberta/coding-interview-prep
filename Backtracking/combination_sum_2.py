class Solution:
    def combinationSum2(self, candidates: List[int], target: int) -> List[List[int]]:

        candidates.sort()
        res = []

        def backtrack(cur, pos, target):
            # i -> the array element index
            # cur -> current combination
            # target -> the target that we're currently trying to sum up to

            # base-case 1
            if target == 0:
                res.append(cur.copy())
            
            # base-case 2
            if target <= 0:
                return

            prev = -1 # default val
            for i in range(pos, len(candidates)):
            
                # to skip the candidate
                if candidates[i] == prev:
                    continue

                cur.append(candidates[i])
                backtrack(cur, i+1, target - candidates[i])
                cur.pop()

                prev = candidates[i]
        
        backtrack([], 0, target)
        return res