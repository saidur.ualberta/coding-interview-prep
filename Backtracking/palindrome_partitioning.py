class Solution:
    def partition(self, s: str) -> List[List[str]]:

        res = [] # final output result

        part = [] # current partition


        def dfs(i):
            # i -> idx to the character we're currently at
            # base-case: if `i` is out of bounds
            if i >= len(s):
                res.append(part.copy())
                return 

            for j in range(i, len(s)):
                # generating every single possible substring
                if self.isPali(s, i, j): # checking if that substring is a palindrome
                    # if palindrome then
                    part.append(s[i:j+1])
                    dfs(j+1)
                    # clean up
                    part.pop()

        dfs(0)
        return res

    def isPali(self, s, l, r):
        while l < r:
            if s[l] != s[r]:
                return False
            l = l + 1
            r = r - 1
        return True

            
