class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:

        result = []

        # base case
        if (len(nums) == 1):
            return [nums[:]]

        for i in range(len(nums)):
            # pop the first value
            n = nums.pop(0)
            # and get permutations of next 2 vals.
            perms = self.permute(nums) # nums has 2 values now

            # add first val to [2,3] [3,2] -> [2,3,1] [3,2,1]

            for perm in perms:
                perm.append(n)

            result.extend(perms) # add multiple vals to list
            # since a val was removed
            nums.append(n) # appended at the end of the array

        return result
